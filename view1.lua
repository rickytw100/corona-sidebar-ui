module(..., package.seeall)
----------------------------------------------------------------------------------
--
-- view1.lua
--
----------------------------------------------------------------------------------


--VARS
local modGroup

function loadView(group)
    modGroup = display.newGroup()
    --ADD UI
    local view1Txt = display.newText(modGroup, "View 1", 0,0, 0, 0, native.systemFontBold, 20)
    view1Txt:setReferencePoint(display.CenterReferencePoint)
    view1Txt.x=(screen.right-screen.left)/2
    view1Txt.y=70
    view1Txt:setTextColor(118,118,118)
    
    group:insert(modGroup)
end

function destroyView()
    print('Destroying view...')
    if(modGroup~=nil)then
        modGroup:removeSelf()
        modGroup=nil 
    end
end


